package models

import (
	"log"
	"os"
	"time"

	_ "github.com/lib/pq"
)

const (
	LabelError = "[ERROR]"
	LabelInfo  = "[INFO] "
)

func LogError(LogType string, path string, context string, errorIn error) {

	currentTime := time.Now()
	datenow := currentTime.Format("2006-01-02")

	f, err := os.OpenFile("./logs/"+datenow+".log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}

	defer f.Close()

	logger := log.New(f, LogType+" ", log.LstdFlags)
	logger.Println(" PATH: "+path+" _____ CONTEXT: "+context+" _____ MESSAGE: ", errorIn)
}

func LogInfo(LogType string, path string, message string, jsonString string) {

	currentTime := time.Now()
	datenow := currentTime.Format("2006-01-02")

	f, err := os.OpenFile("./logs/"+datenow+".log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}

	defer f.Close()

	logger := log.New(f, LogType+" ", log.LstdFlags)
	logger.Println(" PATH: " + path + " _____ MESSAGE: " + message + "_____ INPUT:" + jsonString)
}
