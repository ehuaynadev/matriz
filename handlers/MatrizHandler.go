package handlers

import (
	"net/http"

	repositories "matriz/repositories"

	"github.com/gin-gonic/gin"
)

func GetArrays(c *gin.Context) {

	//cal lógica de negocio
	res, err := repositories.GetLogicArray(c)
	if err != nil {
		c.JSON(
			http.StatusBadRequest,
			gin.H{
				"code":  http.StatusBadRequest,
				"error": err.Error(),
			},
		)
	}
	//response
	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": res})

}
