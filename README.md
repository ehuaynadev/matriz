# matriz

Test desarrollo backend Golang
_Desarrollo de solución para evaluación GOLANG_

 branches
main: gingonic
dev: gorillamnux

### Excepciones 🔧
_Sòlo por esta ocasiòn se està subiendo el archivo .env al repositorio a modo de ejemplo e inmediata ejecuciòn, pues no es una buena pràctica tenerlo en el repositorio.

### Ejecuciòn 🔧

# EJECUCION CON DOCKER
```
docker build  -t matriz .
docker run -d -p 8080:7878 matriz

# ENDPOINT
http://localhost:8080/api/v1/arrays
```

# EJECUCION SIN DOCKER
```
go run *.go
http://localhost:7878/api/v1/arrays

# METHOD
POST
```

**ARRAY 2X2**
_JSON INPUT_
```
    [
        [
            1,
            2
        ],
        [
            3,
            4
        ]
    ]
```
 _JSON OUTPUT_
   
```
   {
    "code": 200,
    "data": [
        [
            2,
            4
        ],
        [
            1,
            3
        ]
    ]
}
```
**_ARRAY 3X3_**
_JSON INPUT_
```
    [
		[
			1,
			2,
			3
		],
		[
			4,
			5,
			6
		],
		[
			7,
			8,
			9
		]
	]
```
_JSON OUTPUT_
```
	
{
    "code": 200,
    "data": [
        [
            3,
            6,
            9
        ],
        [
            2,
            5,
            8
        ],
        [
            1,
            4,
            7
        ]
    ]
}
```

## Lenguaje y herramientas de desarrollo 
* [Golang](https://golang.org/)
* [Visual Studio Code](https://code.visualstudio.com/)

## Author  
* **Edinson Huayna Quispe** - *Web Developer* - [edinsonhq](https://gitlab.com/ehuaynadev) -  [Golang Perú](https://www.facebook.com/Golang-Per%C3%BA-370540473546470)

## Agradecimiento

* Gracias por la oportunidad brindada para participar y espero esta prueba pueda servir a la comunidad golang.
* Saludos

