package main

import (
	"fmt"
	"os"

	"matriz/handlers"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func main() {
	//cargando variables de entorno
	err := godotenv.Load()
	if err != nil {
		fmt.Println(err)
	}

	port := os.Getenv("SERVER_PORT")

	r := gin.Default()

	//routes
	api := r.Group("/api/v1")
	{
		api.POST("/arrays", handlers.GetArrays)
	}

	r.Run(":" + port) // listen and serve on 0.0.0.0:7878

}
